= Training - Playground kotlin: concepts basiques.
@CherOliv
2022-05-14
:jbake-title: Training - Playground kotlin: concepts basiques.
:jbake-type: post
:jbake-tags: blog, ticket, Training, playground
:jbake-status: published
:jbake-date: 2022-05-14
:summary: Playground de programmation en kotlin, ConcatFunctionTest: concepts basiques.


==  ConcatFunctionTest
ExampleUnitTest: https://github.com/cheroliv/playground/blob/master/src/test/kotlin/playground/programming/ConcatFunctionTest.kt[source] +
notions: mémoire, variable, valeur, objet, extension de fonction